# Trombinoscope

Liste des participants à la formation JavaScript et ReactJS.

## Identité

Une identité doit avoir les champs suivants :

- `email`
- `name`
- `team`

e.g.

```json
{
  "mail": "john.doe@cdiscount.com",
  "name": "John Doe",
  "team": "FT-CDISCOUNT"
}
```
